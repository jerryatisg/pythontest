# Python



## concurrency in python

- [A great book on different python topics](https://pymotw.com/3/concurrency.html)
- Different forms of currency in python
  - [Threading](https://gitlab.com/jerryatisg/pythontest/-/blob/main/concurrent.py#L11)
  - [Multi-processing](https://gitlab.com/jerryatisg/pythontest/-/blob/main/concurrent.py#L26)
  - [asyncio](https://gitlab.com/jerryatisg/pythontest/-/blob/main/concurrent.py#L55)
