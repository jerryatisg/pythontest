import platform
import os
import time
import threading
import multiprocessing
import asyncio
# https://pymotw.com/3/concurrency.html


# ---- threading ----
def test_task(count):
    print(f"start task {count} in thread {threading.currentThread().getName()}")
    time.sleep(2)
    print(f"done thread task {count} in thread {threading.currentThread().getName()}")


def test_thread():
    for i in range(4):
        threading.Thread(target=test_task, args=(i,)).start()

    for t in threading.enumerate():
        print(f"found thread {t.getName()}")


# ---- multiprocessing ----
def process_task(count):
    print(f"starting task {count}")
    time.sleep(2)
    print(f"done process task {count} in CPU {multiprocessing.current_process().name} pid:{os.getpid()}")


def calc(i):
    print(f"calculating in {multiprocessing.current_process().name} thread: {threading.currentThread().getName()} pid:{os.getpid()}")
    return i*i*i


def test_process():
    for i in range(multiprocessing.cpu_count()):
        multiprocessing.Process(target=process_task, args=(i,)).start()

    time.sleep(3)

    print("---- test pool ----")
    pool = multiprocessing.Pool(
        processes=10,
        maxtasksperchild=1
    )
    result = pool.map(calc, list(range(10)))
    pool.close()
    pool.join()
    print(result)


# ---- asyncio ----
async def wait_task():
    await asyncio.sleep(2)


async def async_task(count):
    print(f"starting task {count}")
    await wait_task()
    print(f"done  async task {count} in CPU {multiprocessing.current_process().name} thread: {threading.currentThread().getName()} pid:{os.getpid()}")


async def test_aysncio():
    tasks = [async_task(i) for i in range(5)]
    await asyncio.gather(*tasks)


# --- main ----
if __name__ == '__main__':
    print(f"{platform.python_implementation()} {platform.python_version()} {os.name} ")
    print(f"cpu counts: {os.cpu_count()}")
    print(f"pid: {os.getpid()}")
    print("---- Threading test ----")
    test_thread()

    time.sleep(3)

    print("---- multiprocessing test ----")
    test_process()

    time.sleep(3)
    print("---- asyncio test ----")
    loop = asyncio.get_event_loop()
    loop.run_until_complete(test_aysncio())
    loop.close()


